package edu.uga.eits.portal.emergencyContact.rest;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import edu.uga.eits.portal.emergencyContact.orm.Phone;
import edu.uga.eits.portal.emergencyContact.orm.QuickContact;
import edu.uga.eits.portal.emergencyContact.util.DatabaseManager;

/**
 * 
 * @author Yung Long Li
 * 
 * get data from json which is from URL or local file
 */
public class ContactService {
	
	/**
	 * contact cache data
	 */
	private static Map<Integer, QuickContact> contacts = new TreeMap<Integer, QuickContact>();
	
	public static Map<Integer, QuickContact> getContactCache() throws Exception{

	    if (contacts.size() != 0) return contacts;
	    
	    // call hsql procedure
	    Connection conn = DatabaseManager.getConnection();
	    CallableStatement call = conn.prepareCall("call get_contacts()");
	    call.execute();
	    if (call.getMoreResults()){
	        ResultSet rs = call.getResultSet();
	        while (rs.next()) {
	            if (contacts.get(rs.getInt(1)) == null){
	                QuickContact qc = new QuickContact();
	                qc.setName(rs.getString(2));
	                qc.setOrder(rs.getInt(1));
	                Phone p = new Phone(rs.getString(4), rs.getString(5));
	                if (qc.getPhone() == null){
	                    ArrayList<Phone> plist = new ArrayList<Phone>();
	                    plist.add(p);
	                    qc.setPhone(plist);
	                } else {
	                    qc.getPhone().add(p);
	                }
	                contacts.put(rs.getInt(1), qc);
	            } else {
	                QuickContact qc = contacts.get(rs.getInt(1));
	                Phone p = new Phone(rs.getString(4), rs.getString(5));
	                if (qc.getPhone() == null){
                        ArrayList<Phone> plist = new ArrayList<Phone>();
                        plist.add(p);
                        qc.setPhone(plist);
                    } else {
                        qc.getPhone().add(p);
                    }
	            }
	        }
	    }
	    return contacts;
				
	}
	
	public static void updateContactCache() throws SQLException {
	 
	    contacts = new TreeMap<Integer, QuickContact>();
	    // call hsql procedure
        Connection conn = DatabaseManager.getConnection();
        CallableStatement call = conn.prepareCall("call get_contacts()");
        call.execute();
        if (call.getMoreResults()){
            ResultSet rs = call.getResultSet();
            while (rs.next()) {
                if (contacts.get(rs.getInt(1)) == null){
                    QuickContact qc = new QuickContact();
                    qc.setName(rs.getString(2));
                    qc.setOrder(rs.getInt(1));
                    Phone p = new Phone(rs.getString(4), rs.getString(5));
                    if (qc.getPhone() == null){
                        ArrayList<Phone> plist = new ArrayList<Phone>();
                        plist.add(p);
                        qc.setPhone(plist);
                    } else {
                        qc.getPhone().add(p);
                    }
                    contacts.put(rs.getInt(1), qc);
                } else {
                    QuickContact qc = contacts.get(rs.getInt(1));
                    Phone p = new Phone(rs.getString(4), rs.getString(5));
                    if (qc.getPhone() == null){
                        ArrayList<Phone> plist = new ArrayList<Phone>();
                        plist.add(p);
                        qc.setPhone(plist);
                    } else {
                        qc.getPhone().add(p);
                    }
                }
            }
        }

	}
	
	public static void addContact(String name, String phone, String ext) throws Exception{
	    
	    if (contacts == null) getContactCache();
	    Connection conn = DatabaseManager.getConnection();
	    CallableStatement call = conn.prepareCall("call new_contact(?, ?, ?)");
	    call.setString(1, name);
	    call.setString(2, phone);
	    call.setString(3, ext);
	    call.execute();
	    
	}

	public static void main(String[] argus) throws Exception {
		
	    Map<Integer, QuickContact> contacts = ContactService.getContactCache();
        for (QuickContact c : contacts.values()){
            System.out.println("    name = " + c.getName());
            for (int i = 0; i < c.getPhone().size(); i++){
                System.out.print("    phone = " + c.getPhone().get(i).getNumber() + "  ");
                System.out.print(c.getPhone().get(i).getExt());
            }
            System.out.println();
        }
	    
	    addContact("Yung", "706=542-9309", "");
	    
	    updateContactCache();
	    contacts = ContactService.getContactCache();
	    for (QuickContact c : contacts.values()){
            System.out.println("    name = " + c.getName());
            for (int i = 0; i < c.getPhone().size(); i++){
                System.out.print("    phone = " + c.getPhone().get(i).getNumber() + "  ");
                System.out.print(c.getPhone().get(i).getExt());
            }
            System.out.println();
        }
		
	}

}
