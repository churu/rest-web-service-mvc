package edu.uga.eits.portal.emergencyContact.rest;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.TracingConfig;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;

public class RootWebApp extends ResourceConfig {
  
    public RootWebApp()  {
        
        // Resources.
        packages(Contacts.class.getPackage().getName());
        // MVC.
        register(JspMvcFeature.class);
        // Logging.
        register(LoggingFilter.class);
        // Tracing support.
        property(ServerProperties.TRACING, TracingConfig.ON_DEMAND.name());
        
    }
    
    
    
} 