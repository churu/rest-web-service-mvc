package edu.uga.eits.portal.emergencyContact.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * The manager is used to initialize database, import or export data.
 * 
 * @author Yung Long
 * 
 */
public class DatabaseManager {

    protected static Connection conn = null;
    
    /**
     * get the connection of database
     * 
     * @return connection object
     */
    public static Connection getConnection() {

        
        try {
            String _Driver = ConfigTool.getDRIVER();
            try {
                Class.forName(_Driver);
            } catch (Exception e) {
                System.out.println("ERROR: failed to load JDBC driver.");
                e.printStackTrace();
                return null;
            }
            String _user = ConfigTool.getDBUSER();
            String _password = ConfigTool.getDBPASSWORD();
            String _URL = ConfigTool.getDBURL();
            conn = DriverManager.getConnection(_URL, _user, _password);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * close the connection of database
     * 
     */
    public void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            // ignore
        }
    }


    /**
     * test
     * 
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {

        // initial connection
        Connection conn = DatabaseManager.getConnection();
        if (conn != null) {
            System.out.println("Connect to database");
        } else {
            System.out.println("Fail to connect to database");
        }

    }

}
