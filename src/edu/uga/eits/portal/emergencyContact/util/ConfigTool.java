package edu.uga.eits.portal.emergencyContact.util;

import java.util.Properties;

public class ConfigTool {

    private static String DBPASSWORD;
    
    private static String DBUSER;
    
    private static String DBURL;
    
    private static String DRIVER;
    
    /**
     * set all property
     */
    private static void setProperty() {

        ClassLoader loader = ConfigTool.class.getClassLoader();
        
        try {

            Properties prop = new Properties();
            prop.load(loader.getResourceAsStream("Config.properties"));
            // get property and set value
            setDBPASSWORD(prop.getProperty("DBPASSWORD"));
            setDBUSER(prop.getProperty("DBUSER"));
            setDBURL(prop.getProperty("DBURL"));
            setDRIVER(prop.getProperty("DRIVER"));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getDBPASSWORD() {
        if (DBPASSWORD == null) setProperty();
        return DBPASSWORD;
    }

    public static void setDBPASSWORD(String dBPASSWORD) {
        DBPASSWORD = dBPASSWORD;
    }

    public static String getDBUSER() {
        if (DBUSER == null) setProperty();
        return DBUSER;
    }

    public static void setDBUSER(String dBUSER) {
        DBUSER = dBUSER;
    }

    public static String getDBURL() {
        if (DBURL == null) setProperty();
        return DBURL;
    }

    public static void setDBURL(String dBURL) {
        DBURL = dBURL;
    }

    public static String getDRIVER() {
        if (DRIVER == null) setProperty();
        return DRIVER;
    }

    public static void setDRIVER(String dRIVER) {
        DRIVER = dRIVER;
    }

    
}
