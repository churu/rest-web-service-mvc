package edu.uga.eits.portal.emergencyContact.orm;

public class Phone {

	private String number;
	
	private String ext;

	public Phone(String number, String ext){
		this.number = number;
		this.ext = ext;
	}
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}
	
	
}
