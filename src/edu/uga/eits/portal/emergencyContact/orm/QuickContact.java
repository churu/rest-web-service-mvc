package edu.uga.eits.portal.emergencyContact.orm;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.server.mvc.Template;

@Template
@Produces("text/html;qs=5")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class QuickContact {

	private String name;
	
	private ArrayList<Phone> phone;
	
	private int order;
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	public void setOrder(String order) {
		int preferOrder = Integer.parseInt(order);
		this.order = preferOrder;
	}

	public ArrayList<Phone> getPhone() {
		return phone;
	}

	public void setPhone(ArrayList<Phone> phone) {
		this.phone = phone;
	}
		
	public void setPhone(String phoneList) {
		
		ArrayList<Phone> phoneCollection = new ArrayList<Phone>();
		String [] phones = phoneList.split(";");
		for (String phone : phones){
			addPhoneCollection(phone, phoneCollection);
		}
		this.phone = phoneCollection;
	}
	
	/**
	 * 
	 * @param inputText
	 * @param phoneCollection
	 * @throws JSONException 
	 */
	private void addPhoneCollection(String inputText, ArrayList<Phone> phoneCollection) {
		String[] temp = inputText.split("ext");
		if (temp.length == 1){
			phoneCollection.add(new Phone(temp[0], ""));
		}else{
			phoneCollection.add(new Phone(temp[0], temp[1]));
		}
	}

	/**
	 * change format ex. (706) 111-2222
	 * @param phone original phone number ex. 7061112222
	 * @return new format of phone number
	 */
	public void changeFormat(){
		if (phone != null){
			for (Phone p : phone){
				String newNumber = p.getNumber();
				String pre = "(" + p.getNumber().substring(0, 3) + ")";
				String mid = p.getNumber().substring(3, 6);
				String post = p.getNumber().substring(6, 10);
				newNumber = pre + " " + mid + "-" + post;
				p.setNumber(newNumber);
			}
		}
	}

	@GET
    @Produces({MediaType.APPLICATION_XML, MediaType.TEXT_XML, MediaType.APPLICATION_JSON})
    public QuickContact getContact() {
        return this;
    }

}
